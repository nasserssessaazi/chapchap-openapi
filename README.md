# chapchap-openapi

This document specifies the high level design of the chapchap openapi system

## Purpose of the system

awaiting discussion...

## Actors

- chapchap admin
- customer —external entity
- chapchap system

## User Stories

- As a customer, I want to register, so that i can get api credentials for my account
- As a customer, I want to check available products in the chapchap system, so that I can subscribe to those in my interest
- As a customer, I want to be able to subscribe to a product, so that i can transact on the api with that product
- As a customer, I want to be able to post transactions through the api, so that they can be processed by the chapchap system
- As a customer, I want to check all my subscriptions, so that i know the status of my account in the chapchap system
- As a customer, I want to login to the dashboard, so that i can view my transactions in the chapchap system
- As a customer, I want to upload KYC documents to the dashboard so that my registration can be approved by the chapchap admin
- As a chapchap admin, I want to approve subscriptions brought forward by customer, so that they can transact on the chapchap system
- As the chapchap system, it should be able to generate credentials for user after the chapchap admin approves the customer’s KYC, so that the approval process can be marked as complete